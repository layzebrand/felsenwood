#!/usr/bin/python3
# -*- coding: utf-8 -*-


vida = ammo = 0
arma = ''
bag = []


def decor(func):
    def wrap():
        print('===========')
        func()
        print('===========')
    return wrap


@decor
def titulo():
    print('FELSENWOOD')


def clear():
    print("\n" * 100)


def menu():
    while True:

        titulo()
        print('\nDigite a opção desejada: \n')
        print('1 - Novo jogo\n')
        print('2 - Carregar o jogo\n')
        print('3 - Como jogar\n')
        print('4 - Sair\n')

        try:
            op_menu = int(input())
            clear()
            game_on = False
            if op_menu == 1:
                game_on = True
                game()
            elif op_menu == 2:
                if game_on == True:
                    load()
                else:
                    print('Nenhuma partida salva. Inicie um novo jogo.\n')
            elif op_menu == 3:
                howto()
            elif op_menu == 4:
                sair()
            else:
                print("Opção inválida, tente novamente.\n")
                continue
            break
        except ValueError:
            print('Você precisa digitar um número válido.\n')


def howto():
    print('Como Jogar: \n')
    print("Bem-vindo a 'Felsenwood'.")
    print('''
	Esse é um livro-jogo: uma história emocionante se desenrola ao longo de diversas páginas, como num livro comum. A diferença aqui é a interatividade. Nesse mundo, são suas decisões que ditam os rumos da história.\n
	Em Felsenwood, cada página contém o texto que descreve o ambiente e as ações realizadas naquele momento. Abaixo de cada página estão disponíveis uma ou mais opções à sua escolha. Cada uma delas leva a uma ação diversa e um caminho diferente para sua experiência.\n
	O jogo é estruturado de maneira a permitir o avanço e retorno pelo mesmo caminho diversas vezes à medida que você explora o ambiente, encontra itens e define novos objetivos. Dependendo de suas escolhas sua história terá diferentes níveis de triunfo e tragédia, e poderá levá-lo a finais cheios de esperança ou direto para a morte.\n
	Para voltar ao MENU ou para mais informações sobre exploração, combate e dicas para sobreviver e progredir nesse mundo, digite o número correspondente à opção desejada:\n\n''')

    while True:
        print('1 - Instruções')
        print('2 - Recursos')
        print('3 - Armas e vestimentas')
        print('4 - Mundo e itens')
        print('5 - Interface')
        print('6 - Inimigos e combate')
        print('7 - Conquistas e Finais')
        print('8 - Voltar ao Menu')

        try:
            op_howto = int(input())
            clear()
            if op_howto == 1:
                instruct()
            elif op_howto == 2:
                resources()
            elif op_howto == 3:
                weapons()
            elif op_howto == 4:
                itens()
            elif op_howto == 5:
                interface()
            elif op_howto == 6:
                combate()
            elif op_howto == 7:
                finais()
            elif op_howto == 8:
                menu()
            else:
                print('Opção inválida, tente novamente.')
                continue
            break
        except ValueError:
            print('Você precisa digitar um número válido.')


def instruct():
    print('''Instruções:
	Em Felsenwood você assume o papel de um sobrevivente numa cidade devastada por um cataclisma ainda não totalmente compreendido.
	Como regra básica, a observação cuidadosa do ambiente e de suas opções aumenta suas chances de sucesso.\n\n''')


def resources():
    print('''Recursos:
	Você possui dois tipos de recurso:
	- Munição, que permite o uso de armas de fogo;
	- Pontos de vida, que representam sua saúde em geral.
	A munição desse mundo é escassa e é prudente você lembrar disso durante sua exploração.
	Você inicia o jogo com 4 pontos de vida, o que significa que é possível receber até 3 golpes de seus antagonistas - no quarto, seus pontos de vida atingem zero e você morre.
	Não há penalidades para a perda parcial de pontos de vida, mas há somente uma oportunidade para recuperá-los no jogo inteiro - utilizando um item, uma caixa de primeiros socorros.
	Caso você esteja com três ou menos pontos de vida e possua a caixa de primeiros socorros, terá a opção de usá-la a qualquer momento (exceto durante eventos que envolvam dados). Seus pontos de vida sempre serão recuperados até o total de 4.\n\n''')


def weapons():
    print('''Armas e vestimentas:
	Você tem duas opções de armas de fogo:
	- Espingarda;
	- Besta.
	Ambas infligem o mesmo nível de dano e têm a mesma precisão. Suas diferenças são sutis: há alguns pontos específicos do jogo em que o tipo de arna que você escolheu pode fazer a diferença - para o bem ou para o mal.
	Caso sua munição acabe, a única arma disponível é sua boa e velha faca.
	As vestimentas incluem:
	- Colete reforçado;
	- Roupas de caça.
	O colete reforçado fornece UM ponto de vida a mais, elevando o total para 5 (e o número total de golpes que você pode receber, para 4). Ele é destruído no primeiro golpe que você sofrer, e não pode ser reconstruído. O colete também é mais pesado, o que pode representar perigo em alguns momentos do jogo.
	As roupas de caça são leves, não fornecem pontos de vida a mais e não aumentam o risco de morte em alguns eventos.\n\n''')


def itens():
    print('''Mundo e itens:
	Você trafega por uma área não muito distante de seu abrigo, à procura de comida e itens que possam ajudá-lo a sobreviver. A partir do ponto inicial você terá a opção de caminhar na direção dos pontos cardeais e explorar cada setor em detalhes.
	É permitido retornar pelos caminhos já percorridos para nova exploração, entrar em lugares previamente inacessíveis ou mesmo voltar para o abrigo. Exceções a essa regra são os pontos sem retorno próximos aos finais do jogo.
	Há diversos itens espalhados ao longo de Felsenwood, e quase todos são importantes. Encontrá-los e utilizá-los na situação correta é uma das chaves para o sucesso em sua empreitada. Seus itens podem ser checados a qualquer tempo na interface.\n\n''')


def interface():
    print('''Interface:
	COMO FAZER A INTERFACE APENAS COM LINHA DE COMANDO?
	SEMPRE EXIBIR OS STATS (VIDA, MUNIÇÃO E ITENS?) OU
	COLOCAR UMA OPÇÃO DE VER OS STATS AO FINAL DE CADA PÁGINA?''')


def combate():
    print('''Inimigos e combate:
	Num mundo como esse há diversos antagonistas que gostariam de vê-lo a sete palmos de terra. São criaturas de diversos tipos, horrores formados após o cataclisma. Você irá enfrenta-las em diversos momentos durante o jogo.
	 Com ou sem munição, o combate sempre ocorre com a rolagem de um dado comum de seis lados: como regra geral, números maiores aumentam a chance de sucesso.
	 O resultado 6 sempre leva a vitória do combate. O resultado 1 representa sempre erro e implica em nova rolagem de dado - mas antes você deve sobreviver ao ataque da criatura...\n\n''')


def finais():
    print('''Conquistas e finais:
	O jogo possui mais de um final, cada um com níveis crescentes de complexidade e com diferentes implicações sobre o seu lugar neste mundo. Encorajamos você a terminar o jogo em sua totalidade e checar cada uma das conclusões.
	É isso. Vimos os principais aspectos do jogo, mas o melhor ainda está por vir. Um planeta cinzento e perigoso o espera, e todos os seus detalhes e emoções só serão revelados quando você participar deste mundo.\n\n''')


def game():
    nome = input('Qual o seu nome? ')
    clear()
    fome()
    clear()
    sede()


def load():
    pass


def sair():
    fim = input('\nPressione qualquer tecla para finalizar o programa...')
    pass


def fome():
    global vida

    print('''Fome
	Você acorda ouvindo os últimos acordes de um ruído distante. Um som semelhante a rochas rolando, com estrondos intermitentes, parecendo uma avalanche.
	De fato, seu catre treme levemente sob seu corpo, como nos últimos espamos de um terremoto. 
	Sua única certeza: não é novo cataclisma. O mundo não aguentaria um segundo, e você já teria acordado no inferno se fosse o caso.
	O abrigo não permite a entrada de qualquer luz natural. A única iluminação vem de uma fraca lâmpada de LED carregada por energia mecânica - a força de sua mão. Seu catre, o compartimento frio, o armário, alguns galões de água, um galão de diesel e pôsteres rasgados mal colados às paredes compõem todo o resto do ambiente. Você se levanta do catre, espreguiçando, aspirando o ar frio e empoeirado.
	Com passos lentos você se dirige ao compartimento frio. Nada mais é que uma caixa térmica de tamanho médio, que abriga o que você encontra de alimentos e víveres. A abrir a tampa, não há nenhum momtivo para melhorar seu humor: há apenas garrafas vazias. Está na hora de procurar comida. Roedores, pássaros, pacotes de alimentos pré-cataclisma - qualquer coisa tem seu valor.
	Você pega o galão de diesel achando que era o de água, mas percebe o errro e troca os galões a tempo de evitar uma tragédia. Abre a tampa, toma alguns goles e joga um pouco sobre a cabeça e rosto. Gelada. Você passa as mãos pelos cabelos e abre o armário ao lado do catre que contém seus poucos pertences. Hora de decidir como sair.\n\n''')

    while True:
        print('''1 - Pegar seu pesado colete reforçado\n2 - Pegar roupas leves de caça''')
        roupa = input()
        if roupa == '1':
            vida = 5
        elif roupa == '2':
            vida = 4
        else:
            print('Opção inválida, tente novamente.\n')
            continue
        break


def sede():
    global ammo

    print('''Sede
	Você enverga sua vestimenta e afasta a outra porta do armário. Debaixo de alguns trapos está um de seus maiores tesouros: suas armas.\n\n''')
    while True:
        print('1 - Pegar sua espingarda calibre 12')
        print('2 - Pegar sua besta')
        arma = input()
        ammo = 6
        if arma == '1':
            arma = 'espingarda'
        elif arma == '2':
            arma = 'besta'
        else:
            print('Opção inválida, tente novamente.\n')
        break


menu()
