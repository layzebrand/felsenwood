#!/usr/bin/python3
# -*- coding: utf-8 -*-


from random import randint, seed

nome = player = cloth = arma = ''

class Person:
    def __init__(self, name=None):
        self.name = name
        self.life = 4

class Items:
    def __init__(self, wear=None, gun=None):
        self.wear = wear
        self.gun = gun
        self.ammo = 6
        self.vest = False
        self.bag = ['faca']

def decor(func):
    def wrap():
        print('===========')
        func()
        print('===========')
    return wrap


@decor
def titulo():
    print('FELSENWOOD')


def clear():
    print("\n" * 100)


def menu():
    while True:

        titulo()
        print('\nDigite a opção desejada: \n')
        print('1 - Novo jogo\n')
        print('2 - Carregar o jogo\n')
        print('3 - Como jogar\n')
        print('4 - Sair\n')

        try:
            op_menu = int(input())
            clear()
            game_on = False
            if op_menu == 1:
                game_on = True
                game()
            elif op_menu == 2:
                if game_on == True:
                    load()
                else:
                    print('Nenhuma partida salva. Inicie um novo jogo.\n')
            elif op_menu == 3:
                howto()
            elif op_menu == 4:
                sair()
            else:
                print("Opção inválida, tente novamente.\n")
                continue
            break
        except ValueError:
            print('Você precisa digitar um número válido.\n')


def howto():
    print('Como Jogar: \n')
    print("Bem-vindo a 'Felsenwood'.")
    print('''
	Esse é um livro-jogo: uma história emocionante se desenrola ao longo de diversas páginas, como num livro comum. A diferença aqui é a interatividade. Nesse mundo, são suas decisões que ditam os rumos da história.\n
	Em Felsenwood, cada página contém o texto que descreve o ambiente e as ações realizadas naquele momento. Abaixo de cada página estão disponíveis uma ou mais opções à sua escolha. Cada uma delas leva a uma ação diversa e um caminho diferente para sua experiência.\n
	O jogo é estruturado de maneira a permitir o avanço e retorno pelo mesmo caminho diversas vezes à medida que você explora o ambiente, encontra itens e define novos objetivos. Dependendo de suas escolhas sua história terá diferentes níveis de triunfo e tragédia, e poderá levá-lo a finais cheios de esperança ou direto para a morte.\n
	Para voltar ao MENU ou para mais informações sobre exploração, combate e dicas para sobreviver e progredir nesse mundo, digite o número correspondente à opção desejada:\n\n''')

    while True:
        print('1 - Instruções')
        print('2 - Recursos')
        print('3 - Armas e vestimentas')
        print('4 - Mundo e itens')
        print('5 - Interface')
        print('6 - Inimigos e combate')
        print('7 - Conquistas e Finais')
        print('8 - Voltar ao Menu')

        try:
            op_howto = int(input())
            clear()
            if op_howto == 1:
                instruct()
            elif op_howto == 2:
                resources()
            elif op_howto == 3:
                weapons()
            elif op_howto == 4:
                itens()
            elif op_howto == 5:
                interface()
            elif op_howto == 6:
                combate()
            elif op_howto == 7:
                finais()
            elif op_howto == 8:
                menu()
            else:
                print('Opção inválida, tente novamente.')
                continue
            break
        except ValueError:
            print('Você precisa digitar um número válido.')


def instruct():
    print('''Instruções:
	Em Felsenwood você assume o papel de um sobrevivente numa cidade devastada por um cataclisma ainda não totalmente compreendido.
	Como regra básica, a observação cuidadosa do ambiente e de suas opções aumenta suas chances de sucesso.\n\n''')


def resources():
    print('''Recursos:
	Você possui dois tipos de recurso:
	- Munição, que permite o uso de armas de fogo;
	- Pontos de vida, que representam sua saúde em geral.
	A munição desse mundo é escassa e é prudente você lembrar disso durante sua exploração.
	Você inicia o jogo com 4 pontos de vida, o que significa que é possível receber até 3 golpes de seus antagonistas - no quarto, seus pontos de vida atingem zero e você morre.
	Não há penalidades para a perda parcial de pontos de vida, mas há somente uma oportunidade para recuperá-los no jogo inteiro - utilizando um item, uma caixa de primeiros socorros.
	Caso você esteja com três ou menos pontos de vida e possua a caixa de primeiros socorros, terá a opção de usá-la a qualquer momento (exceto durante eventos que envolvam dados). Seus pontos de vida sempre serão recuperados até o total de 4.\n\n''')


def weapons():
    print('''Armas e vestimentas:
	Você tem duas opções de armas de fogo:
	- Espingarda;
	- Besta.
	Ambas infligem o mesmo nível de dano e têm a mesma precisão. Suas diferenças são sutis: há alguns pontos específicos do jogo em que o tipo de arna que você escolheu pode fazer a diferença - para o bem ou para o mal.
	Caso sua munição acabe, a única arma disponível é sua boa e velha faca.
	As vestimentas incluem:
	- Colete reforçado;
	- Roupas de caça.
	O colete reforçado fornece UM ponto de vida a mais, elevando o total para 5 (e o número total de golpes que você pode receber, para 4). Ele é destruído no primeiro golpe que você sofrer, e não pode ser reconstruído. O colete também é mais pesado, o que pode representar perigo em alguns momentos do jogo.  As roupas de caça são leves, não fornecem pontos de vida a mais e não aumentam o risco de morte em alguns eventos.\n\n''')


def itens():
    print('''Mundo e itens:
	Você trafega por uma área não muito distante de seu abrigo, à procura de comida e itens que possam ajudá-lo a sobreviver. A partir do ponto inicial você terá a opção de caminhar na direção dos pontos cardeais e explorar cada setor em detalhes.
	É permitido retornar pelos caminhos já percorridos para nova exploração, entrar em lugares previamente inacessíveis ou mesmo voltar para o abrigo. Exceções a essa regra são os pontos sem retorno próximos aos finais do jogo.
	Há diversos itens espalhados ao longo de Felsenwood, e quase todos são importantes. Encontrá-los e utilizá-los na situação correta é uma das chaves para o sucesso em sua empreitada. Seus itens podem ser checados a qualquer tempo na interface.\n\n''')


def interface():
    print('''Interface:
	COMO FAZER A INTERFACE APENAS COM LINHA DE COMANDO?
	SEMPRE EXIBIR OS STATS (VIDA, MUNIÇÃO E ITENS?) OU
	COLOCAR UMA OPÇÃO DE VER OS STATS AO FINAL DE CADA PÁGINA?''')


def combate():
    print('''Inimigos e combate:
	Num mundo como esse há diversos antagonistas que gostariam de vê-lo a sete palmos de terra. São criaturas de diversos tipos, horrores formados após o cataclisma. Você irá enfrenta-las em diversos momentos durante o jogo.
	 Com ou sem munição, o combate sempre ocorre com a rolagem de um dado comum de seis lados: como regra geral, números maiores aumentam a chance de sucesso.
	 O resultado 6 sempre leva a vitória do combate. O resultado 1 representa sempre erro e implica em nova rolagem de dado - mas antes você deve sobreviver ao ataque da criatura...\n\n''')


def finais():
    print('''Conquistas e finais:
	O jogo possui mais de um final, cada um com níveis crescentes de complexidade e com diferentes implicações sobre o seu lugar neste mundo. Encorajamos você a terminar o jogo em sua totalidade e checar cada uma das conclusões.
	É isso. Vimos os principais aspectos do jogo, mas o melhor ainda está por vir. Um planeta cinzento e perigoso o espera, e todos os seus detalhes e emoções só serão revelados quando você participar deste mundo.\n\n''')


def fome():

    global nome, player, cloth

    print('''FOME
	Você acorda ouvindo os últimos acordes de um ruído distante. Um som semelhante a rochas rolando, com estrondos intermitentes, parecendo uma avalanche.
	De fato, seu catre treme levemente sob seu corpo, como nos últimos espamos de um terremoto. 
	Sua única certeza: não é novo cataclisma. O mundo não aguentaria um segundo, e você já teria acordado no inferno se fosse o caso.
	O abrigo não permite a entrada de qualquer luz natural. A única iluminação vem de uma fraca lâmpada de LED carregada por energia mecânica - a força de sua mão. Seu catre, o compartimento frio, o armário, alguns galões de água, um galão de diesel e pôsteres rasgados mal colados às paredes compõem todo o resto do ambiente. Você se levanta do catre, espreguiçando, aspirando o ar frio e empoeirado.
	Com passos lentos você se dirige ao compartimento frio. Nada mais é que uma caixa térmica de tamanho médio, que abriga o que você encontra de alimentos e víveres. A abrir a tampa, não há nenhum momtivo para melhorar seu humor: há apenas garrafas vazias. Está na hora de procurar comida. Roedores, pássaros, pacotes de alimentos pré-cataclisma - qualquer coisa tem seu valor.
	Você pega o galão de diesel achando que era o de água, mas percebe o errro e troca os galões a tempo de evitar uma tragédia. Abre a tampa, toma alguns goles e joga um pouco sobre a cabeça e rosto. Gelada. Você passa as mãos pelos cabelos e abre o armário ao lado do catre que contém seus poucos pertences. Hora de decidir como sair.\n\n''')

    while True:
        print('''1 - Pegar seu pesado colete reforçado\n2 - Pegar roupas leves de caça''')
        roupa = input()

        cloth = Items()
        if roupa == '1':
            cloth.wear = 'colete'
            cloth.vest = True
            player.life += 1
        elif roupa == '2':
            cloth.wear = 'leve'
        else:
            print('Opção inválida, tente novamente.\n')
            continue
        break


def sede():

    global arma

    print('''SEDE
	Você enverga sua vestimenta e afasta a outra porta do armário. Debaixo de alguns trapos está um de seus maiores tesouros: suas armas.\n\n''')

    while True:
        print('1 - Pegar sua espingarda calibre 12')
        print('2 - Pegar sua besta')
        armamento = input()

        arma = Items()
        if armamento == '1':
            arma.gun = 'shotgun'
        elif armamento == '2':
            arma.gun = 'besta'
        else:
            print('Opção inválida, tente novamente.\n')
        break


def survivor():
    print('''O SOBREVIVENTE
    Você amarra sua arma às costas com uma tira de couro gasto e coloca suas poucas munições no cinto, além do cantil cheio de água. Uma faca afiada é atada com firmeza à bota direita. Sua pequena e gasta mochila de couro está devidamente presa às costas. Você está pronto para explorar mais uma vez o mundo cinzento ao redor.''')


def lar():
    print('''LAR
    Você retira as grossas correntes e empurra a porta do abrigo para o lado esquerdo. À sua frente está um grande monte de objetos - caixas de madeira, mesas velhas, biciletas sem rodas e montes de trapos. São um disfarce necessário, visto não haver tranca do lado de fora. Mesmo com esse artifício seu abrigo já foi invadido duas vezes, com perda de diversos artefatos úteis, e você não tem a menor vontade de uma terceira vez.
    Pacientemente afasta os objetos para o lado, respirando o ar frio enquanto sente o peso da madeira em seus braços. Após cerca de vinte minutos, toda a pilha de tralha está recolocada em frente ao conteiner que você usa como casa.
    Você olha em volta para a vizinhança já conhecida. Um antigo depósito a céu aberto, transformado pelo cataclisma em pilha de metais retorcidos e inúteis. O amplo espaço de concreto estende-se por centenas de metros até um deslfiladeiro do lado leste; ali, o sol vermelho nascce impiedoso entre nuvens cinzentas, cegando seus olhos por um instante. 
    Ao norte uma cerca de arame entrelaçado estende-se por cerca de meia milha, protegendo um pequeno grupo de casas e prédios em pedaços. Ela é abruptamente interrompida pelo grande abismo a leste, e continua do outro lado até uma sinistra torre de vigia a noroeste. A oeste a cerca apresenta várias falhas, e depois caminhos esburacados levam em direção à cidade em ruínas. Ao sul há a continuação do depósito, com diversos conteineres espalhados sobre o concreto sujo.\n\n''')
    home_view()


def round_cerca():
    print('''ARREDORES DA CERCA
    Você está em frente à imensa cerca que limita o perímetro do antigo depósito. Tem altura considerável, mais de dez metros, e estado razoável de conservação. Plantas secas brotam de pedaços de terra em falhas do concreto e espalham-se sem padrão, entrelaçadas com os fios de arame em diversos  pontos. Alguns faróis giratórios, colocados no topo da cerca à guisa de alarme, são agora apenas enfeites. Uma ou outra placa com avisos ilegíveis ainda teima em ficar presa aos losangos de metal.
    Você entrelaça os dedos das mãos nos espaços do arame e observa o redor. A leste o cataclisma destruiu a cerca e tudo que havia no local, deixando de apenas um desfiladeiro imenso e intransponível. Além da cerca estendem-se prédios parcialmente destruídos e ruas estreitas, calçadas de pedra, cheias de buracos, terra e criaturas. Apesar disso, é um dos possíveis campos de caça para alimento.
     À sua esquerda, no final de toda a extensão da cerca, ergue-se a antiga torre de vigia, nunca antes explorada: gritos horríveis podiam, de tempos em tempos, ser ouvidos em suas proximidades. No momento atual a torre parece até convidativa - ainda que ela o chame com a voz da fome.\n\n''')

    while True:

        print('1 - Observar a cerca\n2 - Retornar às proximidades do seu abrigo\n3 - Ir para a torre')
        direcao = input()
        clear()

        if direcao == '1':
            cerca()
        elif direcao == '2':
            print('''CAMINHOS
            Com passo lento e firme, você retorna à frente do abrigo. Seu estômago o lembra da urgência de sua missão.\n\n''')
            home_view()
        elif direcao == '3':
            a=3
        else:
            print('Opção inválida, tente novamente.\n')
            continue
        break


def cerca():
    print('''A CERCA
    A despeito de todo o estrago provocado pelo cataclisma, a cerca que protege o grupamento de casas ao norte permanece imponente por longa extensão. Apesar do topo ser coberto de arame farpado, deve haver pontos de falha e áreas com possibilidade de passagem, o que torna a escalada uma opção. Também é possível caminhar ao longo da cerca na direção leste procurando áreas de falha.\n\n''')

    while True:
        print('1 - Tentar cortar a cerca com a faca\n2 - Observar outras opções\n3 - Escalar a cerca\n4 - Caminhar ao longo da cerca no sentido leste')
        direcao = input()
        clear()
        if direcao == '1':
            clear()
        elif direcao == '2':
            a = 2
        elif direcao == '3':
            homo_erectus()
        elif direcao == '4':
            a=4
        else:
            print('Opção inválida, tente novamente.\n')
            continue
        break


def homo_erectus():
    print('''HOMO ERECTUS
    Você olha para cima, respira fundo e inicia a escalada. Seus dedos agarram os losangos de metal e a ponta das botas se encaixam o melhor possível nos espaços exíguos e amassados. Um braço após o outro, lento e firme. O vento se torna mais forte á medida que você vence os centímetros e metros da cerca.
    Próximo ao topo há um suporte de metal com faroletes quebrados e arame farpado cheio de ferrugem. O tempo abriu algum espaço entre as farpas, mas ainda é necessário cuidado com as mãos. Cortar-se, infectar-se e morrer não está nos seus planos.\n\n''')

    while True:

        print('1 - Passar para o outro lado\n2 - Desistir e descer a cerca')
        direcao = input()
        clear()
        if direcao == '1':
            clear()
            climb_cerca()
        elif direcao == '2':
            clear()
        else:
            print('Opção inválida, tente novamente.\n')
            continue
        break


def climb_cerca():
    print('''DE POEIRA E OUTROS OBSTÁCULOS
    Você agarra a barra metálica no topo da cerca e estica os braços, tentando ficar o mais alto possível. Logo após, ergue a coxa direita e inicia a transição para o outro lado., passando a perna entre as farpas afiadas. O vento faz questão de zunir em seus ouvidos e lembra-lo do perigo que corre. Gotas de suor escorrem pelo seu rosto e secam num segundo. Seus braços tremem com o esforço, mas você se mantém o mais firme possível.
    Uma das pernas já foi. Você está sentado sobre a cerca. O mundo lá embaixo parece pequeno e destruído, mas é para lá que você deve ir.
    Seus braços se retesam mais. Você prende a respiração e ergue a coxa esquerda. Inclina o corpo para o lado. As farpas de metal agitam-se a milímetros do seu rosto.
    De súbito, uma rajada mais forte de vento joga grande quantidade de poeira em seus olhos, ao mesmo tempo que agita o arame farpado. Sua visão falha por um segundo. Suas mãos fraquejam, e você começa a cair.\n\n''')
    rd = input('Agarrar-se à cerca (para rolar o dado pressione ENTER)')
    dice = rolar_dados()
    if dice == 1:
        morte()
    else:
        confirmacao()


def home_view():
    while True:

        print('1 - Ir para a cerca ao norte\n2 - Ir para o desfiladeiro a leste\n3 - Ir para os conteineres ao sul\n4 - Ir para a cidade a oeste\n5 - Continuar próximo à segurança do lar.')
        direcao = input()

        if direcao == '1':
            clear()
            round_cerca()
        elif direcao == '2':
            a = 2
        elif direcao == '3':
            a=3
        elif direcao == '4':
            a=4
        elif direcao == '5':
            clear()
            print('''O SOL ESCARLATE
                Você ri de seu próprio desejo. Sabe muito bem que no que restou do mundo não há segurança - muito menos um lar. Se quiser sobreviver, deve caminhar e lutar.\n\n''')
            continue
        else:
            print('Opção inválida, tente novamente.\n')
            continue
        break


def confirmacao():
    print('''CONFIRMAÇÃO
    Sem enxergar, você projeta o corpo para frente e estica os braços o máximo que pode. Suas mãos deslizam na cerca de metal, trepidantes, mas os dedos conseguem se encaixar em algum espaço e lá ficam. Você fecha as mãos e bate com estrondo contra a cerca. O vento joga poeira em seus ouvidos e agita seus cabelos; na sua boca surge o gosto de sangue.
    Mas você está vivo.
    Após alguns segundos você esfrega os olhos e inicia a lenta descida. Ainda há alguns metros de cerca até o chão. Sua visã, turva como se debaixo d'água, aos poucos se recupera. Por fim, a sensação de solo firme sob seus pés é recebida com alívio, e você deita no solo arenoso para um minuto de descanso.''')



def rolar_dados():
    seed()
    return print('Resultado: {}'.format(randint(1,6)))



def morte():
    print('dead')


def kill():
    print('matou')


def stats():

    global nome, cloth, player, arma

    print('Sobrevivente: {}'.format(str(player.name)))
    print('Vestimenta: {}'.format(str(cloth.wear)))
    print('Vida: {}'.format(str(player.life)))
    print('Armamento: {}'.format(str(arma.gun)))
    print('Munição: {}'.format(str(arma.ammo)))


def game():

    global nome, player

    nome = input('Qual o seu nome? ')
    player = Person(nome)
    clear()
    fome()
    clear()
    sede()
    clear()
    survivor()
    lar()


def load():
    pass


def sair():
    fim = input('\nPressione qualquer tecla para finalizar o programa...')
    pass


if __name__ == '__main__':
    menu()
